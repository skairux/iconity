//
//  AppDelegate.h
//  ICONITY
//
//  Created by Mahefa ANDRIANIFAHANANA on 21/04/15.
//  Copyright (c) 2015 Mahefa ANDRIANIFAHANANA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

